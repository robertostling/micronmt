import re
import random
from operator import itemgetter
from collections import defaultdict

import numpy as np

import efmaral

RE_NONSPACE = re.compile(r'\S+')

def split_tokenizer(s):
    return [(m.group(0), m.start(), m.end()) for m in RE_NONSPACE.finditer(s)]

def read_sents(path, tokenizer):
    with open(path, 'r', encoding='utf-8') as f:
        return [tokenizer(line.rstrip('\n')) for line in f]

def index_sents(sents, prefix=4):
    """Index tokenized sentences for the aligner"""
    index = defaultdict(lambda: len(index))
    if prefix:
        r = tuple(np.array([index[token[:prefix].lower()]
                            for token,_,_ in sent],
                           dtype=np.int32)
                  for sent in sents)
    else:
        r = tuple(np.array([index[token].lower() for token,_,_ in sent],
                           dtype=np.int32)
                  for sent in sents)
    voc = tuple(s for s, idx in sorted(index.items(), key=itemgetter(1)))
    return r, voc, dict(index)

def index_chunks(chunks_list):
    index = defaultdict(lambda: len(index))
    index['</W>']
    index['<W>']
    index['</S>']
    index['<S>']
    r = [[np.array([index[c] for c in chunk], dtype=np.int32)
          for chunk in chunks]
         for chunks in chunks_list]
    voc = tuple(c for c, idx in sorted(index.items(), key=itemgetter(1)))
    return r, voc, dict(index)

class Bitext:
    def __init__(self, src_path, trg_path,
                 src_tokenizer=split_tokenizer,
                 trg_tokenizer=split_tokenizer):
        self.src_sents = read_sents(src_path, src_tokenizer)
        self.trg_sents = read_sents(trg_path, trg_tokenizer)
        assert len(self.src_sents) == len(self.trg_sents)

    def chunks(self, symmetrization='union', threshold=None):
        if threshold is None:
            threshold = 0.5 if symmetrization == 'union' else 0.125
        links = self.union() if symmetrization == 'union' else self.intersect()
        for src_sent, trg_sent, p \
        in zip(self.src_sents, self.trg_sents, links):
            pairs = [(i,j,p[i,j]) for i in range(p.shape[0])
                                  for j in range(p.shape[1])
                                  if p[i,j] >= 0.125]
            used_src = [False]*p.shape[1]
            used_trg = [False]*p.shape[0]
            pairs.sort(key=itemgetter(2), reverse=True)
            src_trg = {}
            trg_src = {}
            for trg_idx, src_idx, _ in pairs:
                if (not used_trg[trg_idx]) and (not used_src[src_idx]):
                    used_trg[trg_idx] = True
                    used_src[src_idx] = True
                    src_trg[src_idx] = trg_idx
                    trg_src[trg_idx] = src_idx

            trg_compressed = {
                    trg_idx: i
                    for i, trg_idx in enumerate(sorted(trg_src.keys()))}
            src_trg_compressed = {
                    src_idx: trg_compressed[trg_idx]
                    for src_idx, trg_idx in src_trg.items()}

            trg_chunks = []
            order = []
            added_trg_idx = []
            for src_idx, src_token in enumerate(src_sent):
                if src_idx in src_trg:
                    trg_idx = src_trg[src_idx]
                    trg_idx_end = trg_idx + 1
                    while trg_idx_end < len(trg_sent) \
                            and not used_trg[trg_idx_end]:
                        trg_idx_end += 1
                    trg_chunks.append(trg_sent[trg_idx:trg_idx_end])
                    order.append(sum(x < trg_idx for x in added_trg_idx))
                    added_trg_idx.append(trg_idx)
                else:
                    trg_chunks.append([])

            src_chunks = [[token] for token in src_sent]

            def make_chunk(tokens):
                return ['<W>']+list(' '.join(s for s,_,_ in tokens))+['</W>']

            yield ([['<S>']] + [make_chunk(src_chunk)
                                for src_chunk in src_chunks] + [['</S>']],
                   [['<S>']] + [make_chunk(trg_chunk)
                                for trg_chunk in trg_chunks] + [['</S>']],
                   order)

    def align(self, n_samplers=16, length=10.0):
        src_sents, src_voc, src_index = index_sents(self.src_sents)
        trg_sents, trg_voc, trg_index = index_sents(self.trg_sents)
        links_forward = efmaral.align_numeric(
                src_sents, trg_sents,
                src_voc, trg_voc,
                src_index, trg_index,
                n_samplers,
                length,
                0.2,            # NULL prior
                0.001,          # lexical prior
                0.001,          # NULL lexical prior
                False,          # reverse direction?
                3,              # full model
                123,            # random seed
                False           # discretize?
                )
        links_reverse = efmaral.align_numeric(
                trg_sents, src_sents,
                trg_voc, src_voc,
                trg_index, src_index,
                n_samplers,
                length,
                0.2,            # NULL prior
                0.001,          # lexical prior
                0.001,          # NULL lexical prior
                False,          # reverse direction?
                3,              # full model
                123,            # random seed
                False           # discretize?
                )
        links_forward = [
                m.reshape((len(trg_sent), len(src_sent)+1))[:,:-1]
                for m, src_sent, trg_sent
                in zip(links_forward, src_sents, trg_sents)]
        links_reverse = [
                m.reshape((len(src_sent), len(trg_sent)+1))[:,:-1]
                for m, src_sent, trg_sent
                in zip(links_reverse, src_sents, trg_sents)]

        self.links_forward = links_forward
        self.links_reverse = links_reverse

    def intersect(self):
        return [m_forward * m_reverse.T for m_forward, m_reverse
                      in zip(self.links_forward, self.links_reverse)]

    def union(self):
        return [m_forward + m_reverse.T for m_forward, m_reverse
                      in zip(self.links_forward, self.links_reverse)]

