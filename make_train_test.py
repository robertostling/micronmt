import sys
import os.path
import random

input_file = sys.argv[1]
train_file = sys.argv[2]
dev_file = sys.argv[3]
test_file = sys.argv[4]
n_dev_lines = int(sys.argv[5])
n_test_lines = int(sys.argv[6])

assert os.path.exists(input_file)
#assert not os.path.exists(train_file)
#assert not os.path.exists(test_file)
assert n_test_lines > 0

with open(input_file, 'rb') as f:
    lines = list(f)

random.seed(123)

indexes = list(range(len(lines)))
available = set(indexes)

test_indexes = set(random.sample(available, min(len(available), n_test_lines)))
available -= test_indexes

dev_indexes = set(random.sample(available, min(len(available), n_dev_lines)))
available -= dev_indexes

train_indexes = available

trainf = open(train_file, 'wb') if train_indexes else None
devf = open(dev_file, 'wb') if dev_indexes else None
testf = open(test_file, 'wb') if test_indexes else None
for i, line in enumerate(lines):
    if i in test_indexes:
        testf.write(line)
    elif i in dev_indexes:
        devf.write(line)
    else:
        trainf.write(line)

for f in (trainf, devf, testf):
    if f is not None: f.close()

