import sys
import pickle

from text import Bitext

def main():
    if len(sys.argv) != 4:
        print('prepare.py source.txt target.txt output.pickle',
              file=sys.stderr)
        return

    print('Reading bitext (assumed to be tokenized)...')
    text = Bitext(sys.argv[1], sys.argv[2])
    print('Word aligning...')
    text.align()
    print('Writing output...')
    with open(sys.argv[3], 'wb') as f:
        pickle.dump(text, f, -1)

if __name__ == '__main__':
    main()

