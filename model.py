from time import time

import numpy as np

import chainer
from chainer import cuda, Variable, training
from chainer import Chain
import chainer.functions as F
import chainer.links as L

class ChunkDecoder(Chain):
    def __init__(self, alphabet_size, embedding_size, state_size, context_size):
        super(ChunkDecoder, self).__init__(
                embedding=L.EmbedID(alphabet_size, embedding_size),
                lstm1=L.LSTM(embedding_size + context_size, state_size),
                output=L.Linear(state_size, alphabet_size))

    def reset_state(self):
        self.lstm1.reset_state()

    def __call__(self, c_t, context):
        return self.output(self.lstm1(
            F.concat((self.embedding(c_t), context), 1)))


class ChunkEncoder(Chain):
    """Encode a character sequence to a vector representation"""
    def __init__(self, alphabet_size, embedding_size, encoding_size):
        state_size = encoding_size // 2
        super(ChunkEncoder, self).__init__(
                embedding=L.EmbedID(alphabet_size, embedding_size),
                forward_lstm=L.LSTM(embedding_size, state_size),
                backward_lstm=L.LSTM(embedding_size, state_size))

    def __call__(self, chunk):
        embedded = F.transpose(self.embedding(chunk), axes=(1,0,2))
        #print('embedded', embedded.data.shape)
        self.forward_lstm.reset_state()
        self.backward_lstm.reset_state()
        forward = [self.forward_lstm(c) for c in embedded]
        backward = [self.backward_lstm(c) for c in embedded[::-1]]
        #print('forward', forward[0].data.shape)
        return F.concat((forward[-1], backward[-1]), 1)


class SentEncoder(Chain):
    def __init__(self, alphabet_size, embedding_size, chunk_encoding_size,
                 encoding_size):
        state_size = encoding_size // 2
        super(SentEncoder, self).__init__(
                chunk_encoder=ChunkEncoder(
                    alphabet_size, embedding_size, chunk_encoding_size),
                forward_lstm=L.LSTM(chunk_encoding_size, state_size),
                backward_lstm=L.LSTM(chunk_encoding_size, state_size))

    def __call__(self, sent):
        self.forward_lstm.reset_state()
        self.backward_lstm.reset_state()
        chunks = [self.chunk_encoder(chunk) for chunk in sent]
        #for i in range(len(sent)):
        #    print(sent[i].data.shape, chunks[i].data.shape)
        #print()
        forward = [self.forward_lstm(v) for v in chunks]
        backward = [self.backward_lstm(v) for v in chunks[::-1]]
        return [F.concat((f, b), 1)  for f, b in zip(forward, backward[::-1])]


class PositionPredictor(Chain):
    def __init__(self, encoding_size, hidden_size):
        super(PositionPredictor, self).__init__(
            hidden=L.Linear(encoding_size*3, hidden_size),
            output=L.Linear(hidden_size, 1))

    def __call__(self, last, partial_sent):
        hidden = [self.hidden(F.concat((last, chunk_tm1, chunk_t), 1))
                  for chunk_tm1, chunk_t
                  in zip(partial_sent, partial_sent[1:])]
        return F.concat([self.output(h) for h in hidden], 1)


class Translator(Chain):
    def __init__(
            self,
            src_alphabet_size,
            trg_alphabet_size,
            char_embedding_size,
            src_encoding_size,
            src_chunk_encoding_size,
            trg_encoding_size,
            trg_chunk_encoding_size,
            position_hidden_size,
            gpu=-1):
        super(Translator, self).__init__(
            sent_encoder = SentEncoder(
                src_alphabet_size,
                char_embedding_size,
                src_chunk_encoding_size,
                src_encoding_size),
            trg_chunk_encoder = ChunkEncoder(
                trg_alphabet_size,
                char_embedding_size,
                trg_chunk_encoding_size),
            position_predictor = PositionPredictor(
                trg_encoding_size,
                position_hidden_size),
            trg_chunk_decoder = ChunkDecoder(
                trg_alphabet_size,
                char_embedding_size,
                trg_chunk_encoding_size//2,
                trg_encoding_size),
            trg_encoder = L.LSTM(
                src_encoding_size + trg_chunk_encoding_size,
                trg_encoding_size),
            trg_empty_chunk_encoding = L.Parameter(
                np.zeros((1, trg_chunk_encoding_size), dtype=np.float32)),
            trg_bos_encoding = L.Parameter(
                np.zeros((1, trg_encoding_size), dtype=np.float32)),
            trg_eos_encoding = L.Parameter(
                np.zeros((1, trg_encoding_size), dtype=np.float32))
            )
        self.gpu = gpu

    def decode(self, src_chunks, trg_index):
        trg_enc = []
        trg_enc_order = []
        trg_chunks = []

        src_enc = self.sent_encoder(src_chunks)

        self.trg_encoder.reset_state()

        trg_chunk_bos = Variable(
                np.array([[trg_index['<S>']]], dtype=np.int32),
                volatile='ON')
        #trg_chunk_eos = Variable(
        #        np.array([[trg_index['</S>']]], dtype=np.int32),
        #        volatile='ON')
        if self.gpu >= 0:
            trg_chunk_bos.to_gpu()
        #    trg_chunk_eos.to_gpu()

        trg_chunk_enc_bos = self.trg_chunk_encoder(trg_chunk_bos)

        #trg_enc_order.append(self.trg_encoder(
        #    F.concat((src_enc[0], trg_chunk_enc_bos))))
        #trg_enc_order.append(self.trg_encoder(
        #    F.concat((src_enc[-1], self.trg_chunk_encoder(trg_chunk_eos)))))

        trg_enc_order = [self.trg_bos_encoding.W, self.trg_eos_encoding.W]
        trg_enc = []
        trg_chunk_enc = trg_chunk_enc_bos

        for i, src_chunk in enumerate(src_chunks[1:-1]):
            trg_enc.append(self.trg_encoder(
                F.concat((src_enc[i+1], trg_chunk_enc))))

            self.trg_chunk_decoder.reset_state()
            c_tm1 = trg_index['<W>']
            trg_chunk = [c_tm1]
            while trg_chunk[-1] != trg_index['</W>'] and len(trg_chunk) < 32:
                c_tm1 = Variable(np.array([c_tm1], dtype=np.int32),
                                 volatile='ON')
                pred_c = F.softmax(self.trg_chunk_decoder(c_tm1, trg_enc[-1]))
                c_t = int(np.argmax(pred_c.data, axis=1))
                trg_chunk.append(c_t)
                c_tm1 = c_t

            is_empty = (len(trg_chunk) == 2)

            trg_chunk_list = trg_chunk
            if is_empty:
                trg_chunk_enc = self.trg_empty_chunk_encoding.W
            else:
                trg_chunk = Variable(np.array([trg_chunk_list],dtype=np.int32),
                                     volatile='ON')
                if self.gpu >= 0:
                    trg_chunk.to_gpu()
                trg_chunk_enc = self.trg_chunk_encoder(trg_chunk)

            if not is_empty:
                if not trg_chunks:
                    trg_chunks.append(trg_chunk_list)
                    assert len(trg_enc_order) == 2
                    trg_enc_order.insert(1, trg_enc[-1])
                else:
                    pred_pos = F.softmax(self.position_predictor(
                            trg_enc[-1], trg_enc_order))
                    best_pos = int(np.argmax(pred_pos.data, axis=-1))
                    trg_chunks.insert(best_pos, trg_chunk_list)
                    trg_enc_order.insert(best_pos+1, trg_enc[-1])
                    #trg_enc.append(self.trg_encoder(
                    #    F.concat((src_enc[i+1], trg_chunk_enc))))
            else:
                pass
                # Empty strings only have to run through the target encoder
                # LSTM, but are not ordered
                #trg_enc.append(self.trg_encoder(
                #    F.concat((src_enc[i+1], trg_chunk_enc))))

        return trg_chunks

    def loss(self, src_chunks, trg_chunks, trg_nonempty, order, verbose=False):
        """
        src_chunks -- list of lists of symbols from source alphabet
        trg_chunks -- list of lists of symbols from target alphabet
        order -- insertion points, not counting the <S> or </S> symbols
                 for instance, mapping
                    <S> a b c </S>
                 to
                    <S> b a c </S>
                 will use order = [0, 0, 2]
                 Note that empty (target) chunks are ignored!
        """
        loss = 0.0

        t0 = time()

        # Encoded source sequence
        src_enc = self.sent_encoder(src_chunks)
        t1 = time()

        # Encoded target chunks (empty ones are None)
        trg_chunk_enc = [self.trg_chunk_encoder(chunk) if nonempty \
                            else self.trg_empty_chunk_encoding.W
                         for nonempty, chunk in zip(trg_nonempty, trg_chunks)]
        t2 = time()
        # trg_enc will contain elements encoded by the target LSTM, one for
        # each source chunk, added in source order
        trg_enc = []

        self.trg_encoder.reset_state()

        trg_enc_order = [self.trg_bos_encoding.W, self.trg_eos_encoding.W]

        order_stack = order[::-1]

        t_src_enc = t1 - t0
        t_trg_chunk_encoder = t2 - t1
        t_trg_encoder = 0.0
        t_trg_chunk_decoder = 0.0
        t_position = 0.0

        for i, src_chunk in enumerate(src_chunks[1:-1]):
            t3 = time()
            # Take one step with the LSTM over encoded source chunks and
            # the encoded previous target chunk
            trg_enc.append(self.trg_encoder(
                F.concat((src_enc[i+1], trg_chunk_enc[i]))))
            
            t4 = time()
            t_trg_encoder += t4 - t3

            # Predict the target chunk (character sequence) from the current
            # target encoder state
            self.trg_chunk_decoder.reset_state()

            trg_chunk = F.transpose(trg_chunks[i+1], axes=(1,0))
            for c_tm1, c_t in zip(trg_chunk, trg_chunk[1:]):
                loss += F.softmax_cross_entropy(
                            self.trg_chunk_decoder(c_tm1, trg_enc[-1]), c_t)

            t5 = time()
            t_trg_chunk_decoder += t5 - t4

            # Predict insertion position from the current target encoder
            # state, but only for non-trivial cases (including the first chunk
            # which is always at relative position 0, and any empty
            # chunk = <W></W> which is not ordered)
            if trg_nonempty[i+1]:
                current_order = order_stack.pop()
                if i >= 1:
                    pred_pos = self.position_predictor(
                            trg_enc[-1], trg_enc_order)
                    j = Variable(np.array([current_order], dtype=np.int32),
                                 volatile='AUTO')
                    if self.gpu >= 0:
                        j.to_gpu()
                    loss += F.softmax_cross_entropy(pred_pos, j)
                trg_enc_order.insert(current_order+1, trg_enc[-1])
            
            t6 = time()
            t_position += t6 - t5

        if verbose:
            print('t(src_enc)           = %.3f' % t_src_enc)
            print('t(trg_chunk_encoder) = %.3f' % t_trg_chunk_encoder)
            print('t(trg_encoder        = %.3f' % t_trg_encoder)
            print('t(trg_chunk_decoder) = %.3f' % t_trg_chunk_decoder)
            print('t(position)          = %.3f' % t_position)
            print(flush=True)

        assert not order_stack

        return loss

