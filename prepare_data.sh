#!/bin/bash

TOKENIZER=~/local/mosesdecoder/scripts/tokenizer/tokenizer.perl
STRIPXML=~/local/mosesdecoder/scripts/generic/strip-xml.perl

# WMT:
# newstest2008  used as dev (en + cs,de,fr,es)
# newstest2009  used as test (en + fr,es)
# newstest2016  used as test (en + cs,de)
# newstest2010-2013  used as train (en + cs,de,fr,es)
#
# Bible:
# Full bible split into 1000 verses dev, 1000 verses test, remaining (ca 28000
# verses) train (en + cs,de,fr,es)
#
# Watchtower:
# Test set of 1000 sentences (en + cs,de,fr,es)
# Dummy training sets available with remaining data.

mkdir -p data/wmt

if [ ! -d "data/dev" ]; then
    wget -O data/dev.tgz http://data.statmt.org/wmt16/translation-task/dev.tgz
    cd data && tar xvzf dev.tgz && cd ..
fi

if [ ! -d "data/test" ]; then
    wget -O data/test.tgz http://data.statmt.org/wmt16/translation-task/test.tgz
    cd data && tar xvzf test.tgz && cd ..
fi

for YEAR in 2010 2011 2012 2013; do
    for FILE_LANG in en de cs es fr; do
        if [ ! -e data/newstest"$YEAR"."$FILE_LANG".tok ]; then
            $TOKENIZER -l $FILE_LANG \
                <data/dev/newstest"$YEAR"."$FILE_LANG" \
                >data/newstest"$YEAR"."$FILE_LANG".tok
        fi
    done
done

for FILE_LANG in en de cs es fr; do
    cat data/newstest201[01]."$FILE_LANG".tok \
        >data/newstest2010-1."$FILE_LANG".tok
    cat data/newstest201[012]."$FILE_LANG".tok \
        >data/newstest2010-2."$FILE_LANG".tok
    cat data/newstest201[0123]."$FILE_LANG".tok \
        >data/newstest2010-3."$FILE_LANG".tok
done

for SOURCE in de cs es fr; do
    mkdir -p data/wmt/"$SOURCE"-en
    cp data/newstest2010-3."$SOURCE".tok \
        data/wmt/"$SOURCE"-en/train."$SOURCE".tok
    cp data/newstest2010-3.en.tok \
        data/wmt/"$SOURCE"-en/train.en.tok
done

for SOURCE in de cs; do
    if [ ! -e data/newstest2016-"$SOURCE"-en."$SOURCE".tok ]; then
        cat data/test/newstest2016-"$SOURCE"en-src."$SOURCE".sgm | \
            $STRIPXML | $TOKENIZER -l $SOURCE \
            >data/newstest2016-"$SOURCE"-en."$SOURCE".tok
    fi
    if [ ! -e data/newstest2016-"$SOURCE"-en.en.tok ]; then
        cat data/test/newstest2016-"$SOURCE"en-ref.en.sgm | \
            $STRIPXML | $TOKENIZER -l en >data/newstest2016-"$SOURCE"-en.en.tok
    fi
    cp data/newstest2016-"$SOURCE"-en."$SOURCE".tok \
        data/wmt/"$SOURCE"-en/test."$SOURCE".tok
    cp data/newstest2016-"$SOURCE"-en.en.tok \
        data/wmt/"$SOURCE"-en/test.en.tok
done

for SOURCE in fr es; do
    if [ ! -e data/wmt/"$SOURCE"-en/test."$SOURCE".tok ]; then
            cat data/dev/newstest2009."$SOURCE" | $TOKENIZER -l $SOURCE \
            >data/wmt/"$SOURCE"-en/test."$SOURCE".tok
    fi
    if [ ! -e data/wmt/"$SOURCE"-en/test.en.tok ]; then
            cat data/dev/newstest2009.en | $TOKENIZER -l en \
            >data/wmt/"$SOURCE"-en/test.en.tok
    fi
done

for SOURCE in de cs fr es; do
    if [ ! -e data/wmt/"$SOURCE"-en/dev."$SOURCE".tok ]; then
            cat data/dev/news-test2008."$SOURCE" | $TOKENIZER -l $SOURCE \
            >data/wmt/"$SOURCE"-en/dev."$SOURCE".tok
    fi
    if [ ! -e data/wmt/"$SOURCE"-en/dev.en.tok ]; then
            cat data/dev/news-test2008.en | $TOKENIZER -l en \
            >data/wmt/"$SOURCE"-en/dev.en.tok
    fi
done

function import_bible {
    mkdir -p data/bible/"$1"-"$3"
    python3 make_aligned.py data/bible/"$1"-"$3" \
        ../paralleltext/bibles/corpus/"$2" \
        ../paralleltext/bibles/corpus/"$4"
    python3 make_train_test.py \
        data/bible/"$1"-"$3"/"$2" \
        data/bible/"$1"-"$3"/train."$1".tok \
        data/bible/"$1"-"$3"/dev."$1".tok \
        data/bible/"$1"-"$3"/test."$1".tok \
        1000 1000
    python3 make_train_test.py \
        data/bible/"$1"-"$3"/"$4" \
        data/bible/"$1"-"$3"/train."$3".tok \
        data/bible/"$1"-"$3"/dev."$3".tok \
        data/bible/"$1"-"$3"/test."$3".tok \
        1000 1000
    rm data/bible/"$1"-"$3"/*.txt
}

import_bible de deu-latn-x-bible-pattloch.txt en eng-latn-x-bible-literal.txt
import_bible cs ces-latn-x-bible-ekumenicky.txt en eng-latn-x-bible-literal.txt
import_bible fr fra-latn-x-bible-darby.txt en eng-latn-x-bible-literal.txt
import_bible es spa-latn-x-bible-nuevointernacional.txt en eng-latn-x-bible-literal.txt

function import_watchtower {
    mkdir -p data/watchtower/"$1"-"$3"
    python3 make_aligned.py data/watchtower/"$1"-"$3" \
        ../paralleltext/watchtower/corpus/"$2"-latn-x-watchtower.txt \
        ../paralleltext/watchtower/corpus/"$4"-latn-x-watchtower.txt
    python3 make_train_test.py \
        data/watchtower/"$1"-"$3"/"$2"-latn-x-watchtower.txt \
        data/watchtower/"$1"-"$3"/train."$1".tok \
        data/watchtower/"$1"-"$3"/dev."$1".tok \
        data/watchtower/"$1"-"$3"/test."$1".tok \
        1000 1000
    python3 make_train_test.py \
        data/watchtower/"$1"-"$3"/"$4"-latn-x-watchtower.txt \
        data/watchtower/"$1"-"$3"/train."$3".tok \
        data/watchtower/"$1"-"$3"/dev."$3".tok \
        data/watchtower/"$1"-"$3"/test."$3".tok \
        1000 1000
    rm data/watchtower/"$1"-"$3"/*.txt
}

import_watchtower de deu en eng
import_watchtower cs ces en eng
import_watchtower fr fra en eng
import_watchtower es spa en eng

mkdir -p data/pickled
for CORPUS in bible wmt; do
    for SOURCE in de cs es fr; do
        if [ ! -e data/pickled/"$CORPUS"-"$SOURCE"-en.pickle ]; then
            python3 prepare.py \
                data/"$CORPUS"/"$SOURCE"-en/train."$SOURCE".tok \
                data/"$CORPUS"/"$SOURCE"-en/train.en.tok \
                data/pickled/"$CORPUS"-"$SOURCE"-en.pickle
        fi
    done
done

mkdir -p data/pickled-joerg
for CORPUS in bible wmt watchtower; do
    for SOURCE in de cs es fr; do
        if [ ! -e data/pickled-joerg/"$CORPUS"-"$SOURCE"-en.pickle ]; then
            python3 prepare.py \
                data-joerg/"$CORPUS"/"$SOURCE"-en/train.tok."$SOURCE" \
                data-joerg/"$CORPUS"/"$SOURCE"-en/train.tok.en \
                data/pickled-joerg/"$CORPUS"-"$SOURCE"-en.pickle
        fi
    done
done

CORPUS=bible
for PART in 10 5 2; do
    for SOURCE in de cs es fr; do
        sed -n '0~'$PART'p' \
            <data-joerg/"$CORPUS"/"$SOURCE"-en/train.tok."$SOURCE" \
            >data-joerg/"$CORPUS"/"$SOURCE"-en/train-"$PART".tok."$SOURCE"
        sed -n '0~'$PART'p' \
            <data-joerg/"$CORPUS"/"$SOURCE"-en/train.tok.en \
            >data-joerg/"$CORPUS"/"$SOURCE"-en/train-"$PART".tok.en
        if [ ! -e data/pickled-joerg/"$CORPUS"-"$PART"-"$SOURCE"-en.pickle ]; then
            python3 prepare.py \
                data-joerg/"$CORPUS"/"$SOURCE"-en/train-"$PART".tok."$SOURCE" \
                data-joerg/"$CORPUS"/"$SOURCE"-en/train-"$PART".tok.en \
                data/pickled-joerg/"$CORPUS"-"$PART"-"$SOURCE"-en.pickle
        fi
    done
done

