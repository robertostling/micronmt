import sys
import pickle

import numpy as np
from chainer import Variable
from chainer import serializers

from model import Translator

def main():
    model = sys.argv[1]
    version = sys.argv[2]
    gpu = -1 # TODO

    with open('%s.pickle' % model, 'rb') as f:
        src_alphabet, src_index, trg_alphabet, trg_index, args = \
                pickle.load(f)

    translator = Translator(
            len(src_alphabet),
            len(trg_alphabet),
            args.char_embedding_size,
            args.source_encoding_size,
            args.source_encoding_size,
            args.target_encoding_size,
            args.target_encoding_size,
            args.position_hidden_size,
            gpu=gpu)

    serializers.load_npz('%s.%s.npz' % (model, version), translator)

    if gpu >= 0:
        translator.to_gpu(gpu)

    def get_sentence(src_tokens):
        src_chunks = [['<S>']] + \
            [['<W>'] + list(token) + ['</W>'] for token in src_tokens] + \
            [['</S>']]
        src_chunks = [[src_index[c] for c in chunk if c in src_index]
                      for chunk in src_chunks]
        src_chunks = [Variable(np.array([chars], dtype=np.int32), volatile='ON')
                      for chars in src_chunks]
        if gpu >= 0:
            for chunk in src_chunks: chunk.to_gpu(gpu)
        return src_chunks

    for line in sys.stdin:
        src_chunks = get_sentence(line.split())
        trg_chunks = translator.decode(src_chunks, trg_index)
        trg_sentence = ' '.join(''.join(trg_alphabet[c] for c in chunk[1:-1])
                                for chunk in trg_chunks[1:-1])
        print(trg_sentence)


if __name__ == '__main__': main()

