import argparse
import sys
import pickle
import random
import time

import numpy as np
from chainer import Variable
from chainer import optimizers, serializers

from text import Bitext, index_chunks
from model import Translator


def main():
    parser = argparse.ArgumentParser(
            description='microNMT -- experimental NMT for small corpora')

    parser.add_argument('--input', type=str, required=True,
            metavar='FILE',
            help='pickled input bitext (crated by prepare.py)')

    parser.add_argument('--model', type=str, required=True,
            metavar='FILE',
            help='model file to write')

    parser.add_argument('--n-heldout-sentences', type=int, default=32,
            metavar='N',
            help='number of held-out sentences for tuning')

    parser.add_argument('--char-embedding-size', type=int, default=64,
            metavar='N',
            help='size of character embeddings')

    parser.add_argument('--source-encoding-size', type=int, default=256,
            metavar='N',
            help='size of source sentence encoding vector')

    parser.add_argument('--target-encoding-size', type=int, default=256,
            metavar='N',
            help='size of target sentence encoding vector')

    parser.add_argument('--position-hidden-size', type=int, default=256,
            metavar='N',
            help='size of hidden layer in position prediction model')

    parser.add_argument('--gpu', type=int, default=-1,
            metavar='N',
            help='use GPU number N (or CPU if negative)')

    args = parser.parse_args()

    print('Loading data...', flush=True, file=sys.stderr)

    with open(args.input, 'rb') as f:
        text = pickle.load(f)

    print('Generating chunks...', flush=True, file=sys.stderr)

    all_sent_pairs = list(text.chunks())

    random.seed(123)
    random.shuffle(all_sent_pairs)

    src_sents, src_alphabet, src_index = index_chunks(
            [src_chunk for src_chunk,_,_ in all_sent_pairs])

    trg_sents, trg_alphabet, trg_index = index_chunks(
            [trg_chunk for _,trg_chunk,_ in all_sent_pairs])

    order_sents = [order for _,_,order in all_sent_pairs]

    all_sent_pairs = [t for t in zip(src_sents, trg_sents, order_sents)
                      if len(t[0]) > 2 and len(t[1]) > 2]
    train_sent_pairs = all_sent_pairs[args.n_heldout_sentences:]
    dev_sent_pairs = all_sent_pairs[:args.n_heldout_sentences]

    translator = Translator(
            len(src_alphabet),
            len(trg_alphabet),
            args.char_embedding_size,
            args.source_encoding_size,
            args.source_encoding_size,
            args.target_encoding_size,
            args.target_encoding_size,
            args.position_hidden_size,
            gpu=args.gpu)

    if args.gpu >= 0:
        translator.to_gpu(args.gpu)

    def get_sentence(src_chunks, trg_chunks, order, volatile='OFF'):
        src_chunks = [Variable(np.array([chunk], dtype=np.int32),
                               volatile=volatile)
                      for chunk in src_chunks]
        trg_chunks = [Variable(np.array([chunk], dtype=np.int32),
                               volatile=volatile)
                      for chunk in trg_chunks]
        trg_nonempty = [len(chunk) != 2 for chunk in trg_chunks]
        if args.gpu >= 0:
            for chunk in src_chunks: chunk.to_gpu(args.gpu)
            for chunk in trg_chunks: chunk.to_gpu(args.gpu)
        return (src_chunks, trg_chunks, trg_nonempty, order)

    log_f = open('%s.log' % args.model, 'w')

    optimizer = optimizers.Adam()
    optimizer.use_cleargrads()
    optimizer.setup(translator)

    n_sentences = 0
    best_dev_loss = float('inf')

    t0 = time.time()
    
    print('Started...', flush=True, file=sys.stderr, end='')

    with open('%s.pickle' % args.model, 'wb') as f:
        pickle.dump((src_alphabet, src_index, trg_alphabet, trg_index,
                     args), f, -1)

    while True:
        random.shuffle(train_sent_pairs)
        for py_src_chunks, py_trg_chunks, py_order in train_sent_pairs:
            translator.cleargrads()
            src_chunks, trg_chunks, trg_nonempty, order = \
                    get_sentence(py_src_chunks, py_trg_chunks, py_order)

            loss = translator.loss(src_chunks, trg_chunks, trg_nonempty, order)
            loss.backward()
            optimizer.update()

            print('.', end='', flush=True, file=sys.stderr)

            n_sentences += 1

            if n_sentences % 1000 == 0:
                dev_loss = 0.0
                for py_src_chunks, py_trg_chunks, py_order in dev_sent_pairs:
                    src_chunks, trg_chunks, trg_nonempty, order = \
                            get_sentence(
                                    py_src_chunks, py_trg_chunks, py_order,
                                    volatile='ON')
                    dev_loss += float(translator.loss(
                        src_chunks, trg_chunks, trg_nonempty, order).data)
                print('%.1f\t%.3f' % (time.time()-t0, dev_loss), file=log_f,
                        flush=True)

                print('\n%.1f: Development loss = %.3f' % (
                        time.time() - t0, dev_loss),
                      flush=True, file=sys.stderr)

                if dev_loss < best_dev_loss:
                    serializers.save_npz(
                            '%s.%d.npz' % (args.model, n_sentences),
                            translator)

if __name__ == '__main__': main()

