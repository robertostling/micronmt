"""Make line-aligned parallel text files from sentence-numbered ones

Usage:
  python3 make_aligned.py output lang1.txt lang2.txt lang3.txt ...

This will create output/lang1.txt output/lang2.txt output/lang3.txt ...
where all sentences that are not in _all_ input files are removed.
"""

import sys
import os.path

directory = sys.argv[1]
filenames = sys.argv[2:]

assert os.path.isdir(directory)

common = None
for filename in filenames:
    with open(filename, 'r') as f:
        sents = {line.split('\t')[0] for line in f}
        if common is None: common = sents
        else: common = common & sents


for filename in filenames:
    with open(filename, 'r') as f, \
         open(os.path.join(directory, os.path.basename(filename)), 'w') as outf:
        for line in f:
            fields = line.rstrip('\n').split('\t')
            if len(fields) == 2 and fields[0] in common:
                print(fields[1], file=outf)

