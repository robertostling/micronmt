# README #

This is an experimental system in Chainer for Neural Machine Translation using small corpora.

### Idea ###

It seems the bottleneck in standard attention-based NMT is the coupling between the source and
target text. If you train such a system on a small training set, you will get a pretty good
language model with little or no connection to the source text.

The idea I'm trying out here is whether we can strengthen the connection by modifying the
following details of the standard NMT approach:

* use word-aligned texts for training
* during generation, run linearly over the *source* rather than the target sentence
* for each source chunk (word or multi-word expression):
    * run a seq2seq model over the source chunk to generate a target chunk
    * predict the insertion position within the partially generated sequence of target chunks (use word alignments for supervision)

Generating acceptable translations on the word level should be fairly easy, and my hope is that
the same goes for predicting the word order. Note that the goal here is to generate acceptable
translations where the standard approach fails to generate anything sensible at all. If you
have tons of data, I don't expect better results from this approach.

### Plan ###

* prototype ready for EMNLP (deadline April 14th)
* evaluate on Bible data + small WMT subsets
* baseline: HNMT(?)